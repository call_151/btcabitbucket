//
//  ViewController.swift
//  BTCA
//
//  Created by call151 on 2018-11-27.
//  Copyright © 2018 call151. All rights reserved.
//

import UIKit
import CoreBluetooth



class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, BluetoothManagerDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var scanningState: UILabel!
    @IBOutlet weak var uartOnlySwtichState: UISwitch!

    let bluetoothManager = BluetoothManager()
    var preferedDevices = PreferedDevices()

    let debug = true
    var selectedPeripheral: CBPeripheral?
    
    // -------------------------------------------------------------------------------bidon 1é206B 
    //MARK: - View SETUP
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        tableView.delegate = self
        tableView.dataSource = self
        bluetoothManager.delegate = self
        ajustUARTOnlySwithAtLoad()
        
        preferedDevices.readPreferedDevice()
        selectTypeOfScanning()
    }

  
    
    func ajustUARTOnlySwithAtLoad(){
        if bluetoothManager.showUARTDeviceOnly == true {
            uartOnlySwtichState.setOn(true, animated: false)
        } else{
            uartOnlySwtichState.setOn(false, animated: false)
        }
        
    }
    
    @IBAction func restartScanning(_ sender: UIButton) {
        bluetoothManager.emptyListAndStartNewScan()
    }

    @IBAction func continuScanning(_ sender: UIButton) {
         bluetoothManager.myContinuScan()
    }
    
    
    @IBAction func uartOnlySwitch(_ sender: UISwitch) {
            if sender.isOn {
                bluetoothManager.showUARTDeviceOnly = true
            } else {
                 bluetoothManager.showUARTDeviceOnly = false
            }
             bluetoothManager.emptyListAndStartNewScan()
    }

    
    
    
    
    // -------------------------------------------------------------------------------
    // MARK: - Scanning
    func selectTypeOfScanning(){
        switch (preferedDevices.UUIDSinString.count) {
        case 0:
            //go scan all   si on a aucun device preferer
            bluetoothManager.emptyListAndStartNewScan()

            //TODO: todo si on a des prefered devices mais aucun present, on doit recommencer mais sans aucun dans le prefered 

        case 1:
            //connect automatically
            let UUIDenString = preferedDevices.UUIDSinString.first!
            if let UUID = UUID(uuidString: UUIDenString) {
                bluetoothManager.myCentralManager.retrievePeripherals(withIdentifiers:[UUID])
                break;
            }else{
                if debug{ print("Impossible Case") }
                fallthrough
            }
                    // should not happen but in case


        default:
            let ArrayOfUUID = preferedDevices.UUIDSinString.map({ UUID.init(uuidString: $0)!  })
            bluetoothManager.myCentralManager.retrievePeripherals(withIdentifiers:ArrayOfUUID)
            break;
        }
    }
    
    
    
    //********************************************************************************
    //**
    //**
    // MARK: - Table View
    //**
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bluetoothManager.peripheralsDiscovered.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellBT", for: indexPath)
        let leNom = (bluetoothManager.peripheralsDiscovered[indexPath.row].name ??  "No Name" )
        cell.textLabel!.text = leNom
        cell.detailTextLabel?.text =  bluetoothManager.peripheralsRSSI[indexPath.row].stringValue
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        if let indexPath = tableView.indexPathForSelectedRow {
            selectedPeripheral = bluetoothManager.peripheralsDiscovered[indexPath.row]
            let leUUIDString = selectedPeripheral?.identifier.uuidString
            
            preferedDevices.addADevice(device: selectedPeripheral!)
            if debug{ print("TEST \(String(describing: leUUIDString))") }
            

            // TODO:  todo on doit le valider si ok, est-il present dans les sauvegarder si oui on le lit, sinon,on l'ajouter dans la lsite des préféré et le sauvegarder sel
        }

    }
    
    // segway EditListOfPreviousDevice
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
        if segue.identifier == "EditListOfPreviousDevice" {
            bluetoothManager.fullStopScaning()
            let controller = (segue.destination as! UINavigationController).topViewController as! EditListOfPreviousDeviceTableViewController
            controller.preferedDevices = preferedDevices
            controller.title = "Edit List" //Previous Device" // TODO: todo CHANGE THAT franco
        }
        
        
        if segue.identifier == "goValidateDevice" {
            bluetoothManager.fullStopScaning()

            if let indexPath = tableView.indexPathForSelectedRow {
                print ("indecPath = \(indexPath) et row = \(indexPath.row)")
            }
            
            let validateDeviceViewController = (segue.destination as! UINavigationController).topViewController as! ValidateDeviceViewController
            validateDeviceViewController.selectedPeripheral = selectedPeripheral
            validateDeviceViewController.myBluetoothManager = bluetoothManager
            validateDeviceViewController.title = "Validate"
            
        }
        

        if debug {print (" debut - out of prepare for xegue")}
    }
    
    
    // BluetoothManagerDelegate
    func BluetoothManagerNewDeviceFound(_ sender: BluetoothManager) {
        self.tableView.reloadData()
        print ("DEvice found v1 ")
    }
    
    func BluetoothManagerNewDeviceFound2(_ sender: BluetoothManager, device: CBPeripheral) {
        if preferedDevices.deviceIsInPreferedDevice(device: device) {
            print ("Device is in the preference decive so tale it v1 \(String(describing: device.name))")
            // TODO - todo segue now --
            selectedPeripheral = device
            performSegue(withIdentifier: "goValidateDevice", sender: nil)
        }
    }
    
    

    func BluetoothManagerLabelChanged(_ sender: BluetoothManager) {
        scanningState.text = bluetoothManager.scanningState
    }
    
    
    
    @IBAction func goBack(segue: UIStoryboardSegue ){
        if debug  { print ("GO BACK - nothing") }
    }
    
    
    

}
