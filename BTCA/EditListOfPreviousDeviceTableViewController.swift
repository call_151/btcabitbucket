//
//  EditListOfPreviousDeviceTableViewController.swift
//  BTCA
//
//  Created by call151 on 2018-12-01.
//  Copyright © 2018 call151. All rights reserved.
//

import UIKit


class EditListOfPreviousDeviceTableViewController: UITableViewController {
    var preferedDevices = PreferedDevices()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        // TODO: - TODO marche pas mais on doit verifer en split view cest important
        self.navigationItem.leftItemsSupplementBackButton = true
        //controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
    }


    // MARK: - Table view data source


    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return preferedDevices.names.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellPreviousDevice", for: indexPath)
        cell.textLabel!.text = preferedDevices.names[indexPath.row]
        cell.detailTextLabel?.text = preferedDevices.UUIDSinString[indexPath.row]
        // Configure the cell...

      //  cell.detailTextLabel?.text =  bluetoothManager.peripheralsRSSI[indexPath.row].stringValue
        
        return cell
    }
    

    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
 

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source            
            preferedDevices.UUIDSinString.remove(at: indexPath.row)
            preferedDevices.names.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
