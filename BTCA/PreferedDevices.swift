//
//  PreferedDevices.swift
//  BTCA
//
//  Created by Call-151 on 2018-12-04.
//  Copyright © 2018 call151. All rights reserved.
//

import Foundation
import CoreBluetooth

let preferedDevicesUUID = "preferedDevicesUUIDString"
let preferedDevicesName = "preferedDevicesName"


class PreferedDevices {
    
    var UUIDSinString = [String]()        //  this is the main item taht i need to check
    var names = [String]()              // need this to disply  in the list

    let userDefaults = UserDefaults.standard
    let debug = true
    
    func readPreferedDevice(){
        UUIDSinString = userDefaults.array(forKey: preferedDevicesUUID) as? [String] ?? [String]()
        names = userDefaults.array(forKey: preferedDevicesName) as? [String] ?? [String]()
    }
    

    func addADevice (device: CBPeripheral ){
        let leUUIDString = device.identifier.uuidString
        
        if !UUIDSinString.contains(leUUIDString) {
            UUIDSinString.append(leUUIDString)
            let leNom = (device.name ??  "No Name" )
            names.append(leNom)
            
            userDefaults.set(UUIDSinString, forKey: preferedDevicesUUID)
            userDefaults.set(names, forKey: preferedDevicesName)
            
            if debug{ print("Adding device to list of prefered device")}
            
//            if !userDefaults.synchronize() { // failed! but not clear what you can do about it
//                if debug{
//                    print("saving user default failed ")
//                }else {
//                     print("saving user default SUCCESS ")
//                }
//            }
        }
    }
    
    
    
    func removeADevice (device: CBPeripheral ){
        
        let leUUIDString = device.identifier.uuidString
        
        if UUIDSinString.contains(leUUIDString) {
            
            if let indexOfDevice = UUIDSinString.index(of: leUUIDString ){
                UUIDSinString.remove(at: indexOfDevice)
                names.remove(at: indexOfDevice)
                
                userDefaults.set(UUIDSinString, forKey: preferedDevicesUUID)
                userDefaults.set(names, forKey: preferedDevicesName)
                
                if debug{ print("Remove device in list of prefered device") }
            }
        }
    }
    
    
    func removeADeviceWithUUIDString (leUUIDString: String ){
        
        if UUIDSinString.contains(leUUIDString) {
            
            if let indexOfDevice = UUIDSinString.index(of: leUUIDString ){
                UUIDSinString.remove(at: indexOfDevice)
                names.remove(at: indexOfDevice)
                
                userDefaults.set(UUIDSinString, forKey: preferedDevicesUUID)
                userDefaults.set(names, forKey: preferedDevicesName)
                
                if debug{ print("Remove device in list of prefered device") }
            }
        }
    }
    
    
    
    
    func deviceIsInPreferedDevice(device:CBPeripheral) -> Bool {
        var found = false
        let leUUIDString = device.identifier.uuidString
        
        if UUIDSinString.contains(leUUIDString) {
            found = true
        }
        return found
    }
    
}
