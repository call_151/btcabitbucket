//
//  CADevice.swift
//  fbCollectionView
//
//  Created by call151 on 2018-11-19.
//  Copyright © 2018 call151. All rights reserved.
//

import Foundation
import CoreBluetooth

let orderSolar = "orderSolarV1"
let orderStandard = "orderStandardV1"

enum TypeOfDevice:String, Codable {
    case  standardV1 = "StandardV1", solarV1 = "SolarV1"
}


class CycleAnalystDevice: Codable {
    
    var uUID: UUID
    var typeOfDevice = TypeOfDevice.standardV1
    var title = [""]
    var unit = [""]
    var dataForFun = [""]   //TODO: todo remove- only for testing wihout CA
    var position = [1]
    var precision = [1]
    var CAVisualArray:[CAVisualElement] = []
    var debug = true
    
    
    init (oneUUID:UUID, oneTypeOfDevice: TypeOfDevice){
        self.uUID = oneUUID
        self.typeOfDevice = oneTypeOfDevice
        self.SetUpinitialArray(typeOfDevice: self.typeOfDevice)
    }
    
    
    // juste de test pour discussion apple 
    static func create(json: Data) -> CycleAnalystDevice? {
        if let newValue = try? JSONDecoder().decode(CycleAnalystDevice.self, from: json){
            return newValue
        }else{
            return nil
        }
    }
    
    var json: Data?{
        print ("inside Data dans Device")
        return try? JSONEncoder().encode(self)
    }
    
    
//    init?(json: Data) {
//        if let newValue = try? JSONDecoder().decode(CADevice.self, from: json){
//            self = newValue
//        } else {
//            return nil
//        }
//    }

    
    func SetUpinitialArray(typeOfDevice: TypeOfDevice){
        
        switch typeOfDevice {
        case TypeOfDevice.solarV1 :
            title = ["GMT+Time","Consomation","V. Battery","A Battery","Speed","Distance","T Motor","RPM","Consomation","Torques","Throttle IN","Throttle Out","PasCertain","Solar","Solar","Direction ","Elevation","Latitude","Longitude ","Error"]
            
            unit = ["Hr-Min-SS","Ah","Volts","Amp","km/hr","km","Celcius","RPM","Watts","newtonMetre","Volts","Volts","Percentage","AH","Amp","Degre","Meter","Degre","Degre","Error"]
            
            dataForFun = ["07:06:05","10","40.2","2","20","2340","32","35","350","15","52","58","22","17","3","359","12","45","78","002"]
            
            position = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19]
            
            precision = [0,1,1,1,1,0,1,0,2,0,1,1,1,1,1,0,0,0,0,0]
        default:
            if debug {print ("nothing yet ")}
        }
        self.createCAVisualElement()
    }
    

    
  // func setUpMofidiedArray
    func createCAVisualElement (){
        for aPosition in position {
            let aCAVisualElement = CAVisualElement(positionInitial: aPosition)
            aCAVisualElement.includedInDisplay = true
            
            CAVisualArray.append(aCAVisualElement)
        }
        
        
        
    }
    
    
    
}
