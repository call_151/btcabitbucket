//
//  ValidateDeviceViewController.swift
//  BTCA
//
//  Created by Call-151 on 2018-12-04.
//  Copyright © 2018 call151. All rights reserved.
//

import UIKit
import CoreBluetooth

class ValidateDeviceViewController: UIViewController , BluetoothManagerPeripheralDidConnectDelegate{

    

    

    var myBluetoothManager: BluetoothManager?
    var selectedPeripheral: CBPeripheral?
    var currentCycleAnalystDevice: CycleAnalystDevice?

    
    @IBOutlet weak var nomLabel: UILabel!
    @IBOutlet weak var uuidLabel: UILabel!
    
    @IBOutlet weak var allLineReceived: UITextView!
    @IBOutlet weak var lastLineReceived: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.navigationItem.leftItemsSupplementBackButton = true
        // Do any additional setup after loading the view.
        nomLabel.text = selectedPeripheral?.name
        uuidLabel.text = selectedPeripheral?.identifier.uuidString
        myBluetoothManager?.delegateForPeripheral = self
        myBluetoothManager!.myCentralManager.connect(selectedPeripheral!, options: nil)
        allLineReceived.text  = ""
        lastLineReceived.text = ""
    }
    
//    func setNotifyValue(_ enabled: Bool, for characteristic: CBCharacteristic){
//
//    }
//
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func BluetoothManagerPeripheralDidConnect(_ sender: BluetoothManager, device: CBPeripheral) {
        print ("je suis dans le protocole dans validate ")
    }


    func BluetoothManagerPeripheralRecievedNewData(_ sender: BluetoothManager, stringReceived: String) {
        allLineReceived.text.append(contentsOf: stringReceived)
    }

    
    @IBAction func sendDataBidon(_ sender: UIButton) {
        myBluetoothManager?.writeValue(data: "francobiz")
    }
    
    
    
}
